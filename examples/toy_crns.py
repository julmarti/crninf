"""Implementation of several toy CRNs."""

import numpy as np


def chain():

    """
    Chain CRN
    MA(1) for A=>B
    MA(1) for B=>C
    MA(1) for C=>D
    MA(1) for D=>E
    """

    nreacs, nspecies = 4, 5
    K = np.ones(nreacs)
    name_species = [chr(ord("A") + i) for i in range(nspecies)]
    y0 = np.array([1, 0, 0, 0, 0])
    skeletons = np.array(
        [[-1, 1, 0, 0, 0], [0, -1, 1, 0, 0], [0, 0, -1, 1, 0], [0, 0, 0, -1, 1]]
    )
    cata = -np.ones(nreacs)
    return (
        skeletons,
        K,
        cata,
        name_species,
        y0,
        lambda y, t: [
            -K[0] * y[0],
            K[0] * y[0] - K[1] * y[1],
            K[1] * y[1] - K[2] * y[2],
            K[2] * y[2] - K[3] * y[3],
            K[3] * y[3],
        ],
    )


def reactant_parallel():

    """
    Reactant Parallel CRN
    MA(3) for A+D=>B+D
    MA(2) for C=>D
    MA(1) for E=>D
    """

    K = np.array([2, 1, 3])
    nspecies = 5
    name_species = [chr(ord("A") + i) for i in range(nspecies)]
    y0 = np.array([1, 0, 1, 0, 1])
    skeletons = np.array([[0, 0, -1, 1, 0], [0, 0, 0, 1, -1], [-1, 1, 0, 0, 0]])
    cata = np.array([-1, -1, 3])
    return (
        skeletons,
        K,
        cata,
        name_species,
        y0,
        lambda y, t: [
            -K[2] * y[0] * y[3],
            K[2] * y[0] * y[3],
            -K[0] * y[2],
            K[0] * y[2] + K[1] * y[4],
            -K[1] * y[4],
        ],
    )


def product_parallel():

    """
    Product Parallel CRN
    MA(1) for A+C=>B+C
    MA(2) for C=>D
    MA(3) for C=>E
    """

    K = np.array([1, 2, 3])
    nspecies = 5
    name_species = [chr(ord("A") + i) for i in range(nspecies)]
    y0 = np.array([1, 0, 1, 0, 0])
    skeletons = np.array([[0, 0, -1, 1, 0], [0, 0, -1, 0, 1], [-1, 1, 0, 0, 0]])
    cata = np.array([-1, -1, 2])
    return (
        skeletons,
        K,
        cata,
        name_species,
        y0,
        lambda y, t: [
            -K[0] * y[0] * y[2],
            K[0] * y[0] * y[2],
            -(K[1] + K[2]) * y[2],
            K[1] * y[2],
            K[2] * y[2],
        ],
    )


def loop():

    """
    Loop CRN
    MA(1) for A=>B
    MA(1) for B=>C
    MA(1) for C=>D
    MA(1) for D=>E
    MA(1) for E=>A
    """

    nreacs, nspecies = 5, 5
    K = np.ones(nreacs)
    name_species = [chr(ord("A") + i) for i in range(nspecies)]
    y0 = np.array([1, 0, 0, 0, 0])
    skeletons = np.array(
        [
            [-1, 1, 0, 0, 0],
            [0, -1, 1, 0, 0],
            [0, 0, -1, 1, 0],
            [0, 0, 0, -1, 1],
            [1, 0, 0, 0, -1],
        ]
    )
    cata = -np.ones(nreacs)
    return (
        skeletons,
        K,
        cata,
        name_species,
        y0,
        lambda y, t: [
            K[4] * y[4] - K[0] * y[0],
            K[0] * y[0] - K[1] * y[1],
            K[1] * y[1] - K[2] * y[2],
            K[2] * y[2] - K[3] * y[3],
            K[3] * y[3] - K[4] * y[4],
        ],
    )


def lv():

    """
    Lokta Volterra CRN
    MA(0.3) for A=>_
    MA(1) for B=>2B
    MA(0.01) for A+B=>2A
    """

    _, nspecies = 3, 2
    K = np.array([0.3, 1, 0.01])
    name_species = [chr(ord("A") + i) for i in range(nspecies)]
    y0 = np.array([50, 100])
    skeletons = np.array([[-1, 0], [0, 1], [1, -1]])
    cata = np.array([-1, 1, 0])
    return (
        skeletons,
        K,
        cata,
        name_species,
        y0,
        lambda y, t: [
            -K[0] * y[0] + K[2] * y[0] * y[1],
            -K[2] * y[0] * y[1] + K[1] * y[1],
        ],
    )


def mapk():

    """
    MAPK cascade simplified to its first two levels
    adapted from Qiao et al. Bistability and Oscillations
    in the Huang-Ferrell model of MAPK signaling,
    PLOS comp. biol. 3(9) 2007 e184

    MA(0.0045) for A => Ap.

    MA(1000) for Ap+B => ApB.
    MA(150) for ApB => Ap+B.
    MA(150) for ApB => Ap+Bp.

    MA(1000) for Ap+Bp => ApBp.
    MA(150) for ApBp => Ap+Bp.
    MA(150) for ApBp => Ap+Bpp.
    """

    K = np.array([0.0045, 1000, 150, 150, 1000, 150, 150])
    name_species = [
        "Bpp",
        "Ap",
        "ApBp",
        "Bp",
        "ApB",
        "B",
        "A",
    ]
    y0 = np.array([0, 0, 0, 0, 0, 1.2, 0.003])
    skeletons = np.zeros((7, 7))
    skeletons[0, [6, 1]] = [-1, 1]
    skeletons[1, [1, 5, 4]] = [-1, -1, 1]
    skeletons[2, [4, 1, 5]] = [-1, 1, 1]
    skeletons[3, [4, 1, 3]] = [-1, 1, 1]
    skeletons[4, [1, 3, 2]] = [-1, -1, 1]
    skeletons[5, [2, 1, 3]] = [-1, 1, 1]
    skeletons[6, [2, 1, 0]] = [-1, 1, 1]

    cata = -np.ones(7)
    return (
        skeletons,
        K,
        cata,
        name_species,
        y0,
        lambda y, t: [
            y[2] * K[6],
            y[6] * K[0]
            + y[4] * K[2]
            + y[4] * K[3]
            + y[2] * K[5]
            + y[2] * K[6]
            - y[5] * y[1] * K[1]
            - y[1] * y[3] * K[4],
            y[1] * y[3] * K[4] - y[2] * K[5] - y[2] * K[6],
            y[4] * K[3] + y[2] * K[5] - y[1] * y[3] * K[4],
            y[5] * y[1] * K[1] - y[4] * K[2] - y[4] * K[3],
            y[4] * K[2] - y[5] * y[1] * K[1],
            -y[6] * K[0],
        ],
    )


def func_crn_name(crn_name):

    """
    Function
    - Input:
        * crn_name: str, name of the crn among 'chain', 'loop', 'parallel',
        'product_parallel', 'reactant_parallel', 'complexation', 'double_output'.
    - Output:
        * callable to the associated crn function.
    """

    return globals()[crn_name]
