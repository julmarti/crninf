"""Inference for a particular example toy crn."""

import pickle as pkl

import numpy as np
from scipy.integrate import odeint
from toy_crns import func_crn_name

from crninf.algorithm import Reactmine
from crninf.evaluate import f1_score
from crninf.utils import (
    low_complexity_crn,
    merge,
    parser_example,
    print_crn,
    remove_ABBA,
)


def main(
    crn_name,
    alpha,
    delta_max,
    gamma,
    beta,
    npts,
    time,
    verbose=False,
    save=False,
    kinetics="mal",
):

    # Generate simulation
    t = np.linspace(0, time, npts)

    gt_crn, _, cata, species_names, y0, ode = func_crn_name(crn_name)()
    exps = odeint(ode, y0, t, atol=10 ** (-12), rtol=10 ** (-12))

    algo = Reactmine(
        [exps],
        v=None,
        t=[t],
        species_names=species_names,
        alpha=alpha,
        delta_max=delta_max,
        gamma=gamma,
        beta=beta,
        verbose=verbose,
        kinetics=kinetics,
    )

    # crnlist ranked from lowest to highest error
    _, crnlist, errors = algo.fit()
    if len(crnlist):
        f1s = [f1_score(merge(remove_ABBA(crn)), gt_crn, cata) for crn in crnlist]
        idxf1 = np.argmax([f[-1] for f in f1s])
        # first crn of the results list is the one that maximizes the F1-score
        results = [[crnlist[idxf1], errors[idxf1], *f1s[idxf1]]]

        size = len(crnlist)
        # otherwise, these can be used to prune the low-ranked CRNs.
        # size_quantile = np.searchsorted(energies, np.quantile(energies, .5))
        # size_10energy =  np.searchsorted(energies, 10*energies[0])
        # size = np.minimum(size_quantile, size_10energy)

        crnlist, errors = crnlist[:size], errors[:size]
        results += [[crn, e, *f] for (crn, e, f) in zip(crnlist, errors, f1s)]

        lowcomp_crn, lowcomp_idx = low_complexity_crn(crnlist, errors)

        print("\nlow complexity crn inferred:")
        print_crn(lowcomp_crn, species_names)
        print(
            f"Optimization error: {errors[lowcomp_idx]}, F1-Score, precision, recall: {f1s[lowcomp_idx]}\n"
        )

        print("lowest error crn inferred:")
        print_crn(crnlist[0], species_names)
        print(f"Optimization error: {errors[0]}, F1-Score, precision, recall: {f1s[0]}")

    else:
        results = []

    if save:
        with open(f"{save}.dat", "wb") as f:
            pkl.dump(results, f)


if __name__ == "__main__":
    parser = parser_example()
    main(**vars(parser.parse_args()))
