"""Inference for a particular example toy crn. This file is called by run_all.py for gridsearch/sensitivity analyses."""

import pickle as pkl

import numpy as np
from scipy.integrate import odeint
from toy_crns import func_crn_name

from crninf.algorithm import Reactmine
from crninf.evaluate import f1_score
from crninf.utils import merge, parser_example, remove_ABBA


def main(
    crn_name,
    alpha,
    delta_max,
    gamma,
    beta,
    npts,
    time,
    verbose=False,
    save=False,
    kinetics="mal",
):

    # Generate some simulations
    t = np.linspace(0, time, npts)

    gt_crn, _, cata, name_species, y0, ode = func_crn_name(crn_name)()
    exps = odeint(ode, y0, t, atol=10 ** (-12), rtol=10 ** (-12))

    algo = Reactmine(
        [exps],
        v=None,
        t=[t],
        species_names=name_species,
        alpha=alpha,
        delta_max=delta_max,
        gamma=gamma,
        beta=beta,
        kinetics=kinetics,
        verbose=verbose,
    )

    _, crnlist, energies = algo.fit()
    if len(crnlist):
        f1s = [f1_score(merge(remove_ABBA(crn)), gt_crn, cata) for crn in crnlist]
        idxf1 = np.argmax([f[-1] for f in f1s])
        results = [[crnlist[idxf1], energies[idxf1], *f1s[idxf1]]]

        # we do not save all crns for storage issues
        size_quantile = np.searchsorted(energies, np.quantile(energies, 0.5))
        size_10energy = np.searchsorted(energies, 10 * energies[0])
        size = np.minimum(size_quantile, size_10energy)

        crnlist, energies = crnlist[:size], energies[:size]
        results += [[crn, e, *f] for (crn, e, f) in zip(crnlist, energies, f1s)]
    else:
        results = []

    if save:
        with open(f"{save}.dat", "wb") as f:
            pkl.dump(results, f)


if __name__ == "__main__":
    parser = parser_example()
    main(**vars(parser.parse_args()))
