import functools
import itertools
import multiprocessing as mp
import os
import subprocess

from crninf.utils import parser_example


def eval_model(combi, save):

    crn, alpha, beta, gamma = combi
    if crn in ["reactant_parallel", "product_parallel", "chain", "loop"]:
        npts, T = 101, 10
    elif crn == "mapk":
        npts, T = 301, 100

    subprocess.call(
        [
            f"python3 run_example_fast.py -c {crn} -a {alpha} -g {gamma} -b {beta} -d 3 -s {save}/{alpha}_{beta}_{gamma}_{npts}_{crn} -t {T} -npts {npts}"
        ],
        shell=True,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )


def parallel_eval(combi, save, x):
    return eval_model(combi[x], save)


def main(save, **args):

    try:
        os.mkdir(save)
    except FileExistsError:
        pass

    crns = ["reactant_parallel", "product_parallel", "chain", "loop"]

    # delta_max is fixed to 3 (default value) in all experiments
    alphas = [0.005, 0.01, 0.02, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.5]
    beta = [4, 5, 6, 7, 8]
    gammas = [[3, 4, 5, 6], [3, 4, 5, 6], [4, 5, 6], [5, 6]]
    combi_toy = []
    for i, gamma in enumerate(gammas):

        li = [[crns[i]], alphas, beta, gamma]
        combi_toycrn = list(itertools.product(*li))
        combi_toycrn = sorted(combi_toycrn, key=lambda e: (e[0], e[-1]))
        combi_toy = combi_toy + combi_toycrn

    # for mapk
    alphas = [
        0.0025,
        0.005,
        0.0075,
        0.01,
        0.015,
        0.02,
        0.025,
        0.03,
        0.04,
        0.05,
        0.1,
        0.2,
    ]
    beta = [6, 8, 10]
    gamma = [7, 8, 9, 10]

    li_mapk = [["mapk"], alphas, beta, gamma]
    combi_mapk = list(itertools.product(*li_mapk))
    combi_mapk = sorted(combi_mapk, key=lambda e: (e[0], e[-1]))

    combi = combi_toy + combi_mapk
    pool = mp.Pool()
    _ = pool.map(functools.partial(parallel_eval, combi, save), range(len(combi)))


if __name__ == "__main__":
    parser = parser_example()
    main(**vars(parser.parse_args()))
