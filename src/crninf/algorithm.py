import copy

import matplotlib.pyplot as plt
import numpy as np
from findiff import FinDiff
from scipy.optimize import minimize

from crninf.utils import adapt_save_fig, str_reaction


class Reactmine:
    def __init__(
        self,
        y,
        t,
        v=None,
        species_names=None,
        alpha=0.1,
        delta_max=3,
        delta_step=0.1,
        beta=6,
        gamma=6,
        verbose=0,
        kinetics="mal",
    ):

        """
        Constructor of the Reactmine class.
        - Input:
            * y: list of numpy matrices, experiments, with same number of species.
            * v: velocities, can be given precomputed as a list of numpy arrays y-sized.
            * t: list of time grids, same length as y
            * species_names: list, name for the species.
            * alpha: positive float, CV-based reaction acceptance threshold.
            * delta_max: float > 1, maximum threshold for species variations
            pairing.
            * delta_step: step for the grid of delta values to be tried.
            * beta: int, number of candidate reactions inferred.
            * gamma: int, limit size of the inferred CRNs.
            * verbose: boolean, whether or not to print information.
            * kinetics: str, "mal" or "mm", kinetic rate function searched for.
        """

        self.verbose = verbose

        self.alpha = alpha
        self.delta_step = delta_step
        # delta: grid of species variations pairing values
        self.delta = np.arange(1, delta_max + delta_step, delta_step)
        self.beta = beta
        self.gamma = gamma
        self.kinetics = kinetics

        self.t = t
        # multiple traces are stacked in a single matrix
        self.vinit = np.vstack(self.get_v(y) if v is None else v)
        self.v = np.copy(self.vinit)
        self.y = np.vstack(y)

        self.species_names = (
            [chr(ord("A") + i) for i in range(y[0].shape[1])]
            if species_names is None
            else species_names
        )
        self.nspecies = len(self.species_names)
        self.crn = {}

    def get_v(self, y):

        """
        Compute the discrete derivatives via finite differences.
        - Input:
            * y: list of numpy matrices, experiments, with same number of species.
            * t: list of numpy arrays, time grids, with same length as y.
        - Output:
            * v, list of numpy matrices, same dimensions as y, estimated velocities.
        """

        if len(self.t) == 1:
            d_dt = FinDiff(1, self.t[0])
            v = [d_dt(trace.T).T for trace in y]
        else:
            assert len(self.t) == len(
                y
            ), "number of time grids provided does not match number of traces"
            v = []
            for i in range(len(self.t)):
                d_dt = FinDiff(1, self.t[i])
                v.append(d_dt(y[i].T).T)
        return v

    def get_kinetics(self, skeleton, v, support, cata=-1):

        """
        Associate kinetics to a reaction skeleton given observed velocities.
        - Input:
            * skeleton: numpy array of size nspecies.
                        Entries are -1 for a reactant and 1 for a product.
            * v, numpy array, matrix of velocities.
            * support: list of time points indices where skeleton was witnessed.
            * cata: catalyst species, default value of -1 for no catalyst.
        - Output:
            * reac: dict with reaction CV, MAL or MM
            parameter, skeleton, catalyst, effect and support.
        """

        r = np.where(skeleton == -1)[0]
        rcata = np.append(r, [cata]) if cata != -1 else r
        km = 0

        if self.kinetics == "mm" and len(r) == 1:
            vmax = np.abs(self.v[:, r]).max()
            maxgap = (np.abs(np.abs(self.v[:, r].ravel()) - vmax / 2)).argmin()
            km = self.y[maxgap, r][0]

        v_supp, effect_supp = (
            v[support],
            self.kinetics_function(rcata, km)[support].ravel(),
        )
        cv, k = np.inf, 0

        for s in np.where(skeleton)[0]:
            k_dist_s = skeleton[s] * np.divide(
                v_supp[:, s],
                effect_supp,
                out=1e100 * np.ones_like(support),
                # prevent numerical instabilities
                where=effect_supp > 1e-8,
            )
            k_dist_s = k_dist_s[np.abs(k_dist_s) != 1e100]
            k_mean_s, cv_species = get_mean_cv(k_dist_s)
            if cv_species < cv and k_mean_s > 0:
                cv = cv_species
                k = k_mean_s

        return {
            "cv": cv,
            "kinetics": k,
            "skeleton": skeleton,
            "cata": cata,
            "effect": (self.kinetics_function, rcata),
            "supp": support,
            "km": km,
        }

    def transition_update(self, reac, v):

        """
        Return a copy of the input velocities with the reaction passed
        as input removed.
        - Input:
            * reac: dict outputted by get_kinetics
            * v, numpy array, matrix of velocities.
        - Output:
            * vupdate: numpy array, matrix of updated velocities.
        """

        vupdate = np.copy(v)
        effect = reac["effect"][0](reac["effect"][1], reac["km"])
        vupdate -= effect * reac["skeleton"] * reac["kinetics"]
        return vupdate

    def fit(self):

        """
        Main loop of the research tree.
        - Output:
            * tree: list of lists with each reaction inferred by tree level.
            * crnlist: list of crn dicts.
            * energies: list of associated energies
        """

        level1 = self.grow()
        for i1 in range(len(level1)):
            # idx will allow to track where we are in the tree
            level1[i1]["idx"] = [i1]
        tree, crnlist, nextlevel = [level1], [], [1]

        while len(tree) < self.gamma and len(nextlevel):
            if self.verbose:
                print(f"currently exploring level: {len(tree)+1}")
            currentlevelskels, nextlevel, width = [], [], 0
            for r1 in tree[-1]:
                current_crn = [tree[j1][k] for j1, k in enumerate(r1["idx"])]
                current_skel = [
                    np.append(r["skeleton"], r["cata"]).tolist() for r in current_crn
                ]
                # check if CRN already inferred but not in same order
                if sorted(current_skel) in [sorted(c) for c in currentlevelskels]:
                    continue
                else:
                    currentlevelskels.append(current_skel)
                candid = self.grow(current_skel, current_crn)
                # first way to end a path: no reaction good enough anymore
                if not candid:
                    crnlist.append(current_crn)
                else:
                    for i2 in range(len(candid)):
                        candid[i2]["idx"] = copy.deepcopy(r1["idx"]) + [width]
                        width += 1
                        nextlevel.append((candid[i2]))
                        # second way: CRN maximal size reached
                        if len(current_crn) == self.gamma - 1:
                            copy_crn = copy.deepcopy(current_crn)
                            copy_crn.append(candid[i2])
                            new_current_skel = [
                                np.append(r["skeleton"], r["cata"]).tolist()
                                for r in copy_crn
                            ]
                            if sorted(new_current_skel) in [
                                sorted(c) for c in currentlevelskels
                            ]:
                                continue
                            else:
                                currentlevelskels.append(new_current_skel)
                                crnlist.append(copy_crn)
            tree.append(nextlevel) if nextlevel else None

        if self.verbose:
            print("final globopt of all inferred CRNs")
        energies = np.empty(len(crnlist))
        for i in range(len(crnlist)):
            crnlist[i], energies[i] = (
                self.globopt(crnlist[i])
                if self.kinetics == "mal"
                else self.globopt_mm(crnlist[i])
            )

        crndictlist = [
            {f"reaction {j}": crn[j] for j in range(len(crn))} for crn in crnlist
        ]
        sort_idx = np.argsort(energies)
        return tree, [crndictlist[i] for i in sort_idx], [energies[i] for i in sort_idx]

    def grow(self, skels=[], crn={}):

        """
        Infer the beta next reactions from input velocities.
        - Input:
            * v: numpy array, matrix of velocities.
            * skels: list of already inferred reaction skeleton for the path considered.
            * skels: list of already inferred reaction for the path considered.
        - Output:
            * list of at most beta candidate reaction dicts.
        """

        v = np.copy(self.vinit)
        for i in range(len(crn)):
            v = self.transition_update(crn[i], v)
        skeleton_matrix, support = get_skeletons(v, self.delta)
        if skeleton_matrix.size:
            best = [
                self.get_kinetics(skeleton, v, support[tuple(skeleton)])
                for skeleton in skeleton_matrix
            ]
            best_score = min([best[i]["cv"] for i in range(min(self.beta, len(best)))])
        else:
            return []

        # try catalyst if no reaction w/ low enough cv
        if best_score > self.alpha and self.kinetics == "mal":
            for skeleton in skeleton_matrix:
                # do not search for catalyst for bimolecular reactions.
                if len(skeleton[skeleton < 0]) < 2:
                    # prevent considering 2A=>B (could be removed)
                    for c in [s for s in range(self.nspecies) if skeleton[s] != -1]:
                        best.append(
                            self.get_kinetics(skeleton, v, support[tuple(skeleton)], c)
                        )
        reacs = [
            b
            for b in best
            if np.append(b["skeleton"], b["cata"]).tolist() not in skels
            and b["cv"] < self.alpha
            and b["kinetics"]
        ]
        # lexicographic order: CV then decreasing support size
        return sorted(reacs, key=lambda e: (e["cv"], -len(e["supp"])))[: self.beta]

    def globopt(self, crn):

        """
        Global optimization performed after a CRN has been obtained from
        research tree.
        - Input:
            * crn outputted by research tree.
        - Output:
            * crn with optimized parameters.
            * res.fun: quadratic loss after parameter fitting.
        """

        nreacs = len(crn)
        S, K, effect = self.get_energy_quantities(crn)
        bounds = tuple([(0, None) for _ in range(nreacs)])

        if nreacs:
            res = minimization(K, self.vinit, effect, S, bounds)
            if res.message == "ABNORMAL_TERMINATION_IN_LNSRCH":
                res = minimization(
                    K, self.vinit, effect, S, bounds, method="trust-constr"
                )

        copy_crn = copy.deepcopy(crn)
        for i in range(len(copy_crn)):
            copy_crn[i]["old_kinetics"] = np.copy(copy_crn[i]["kinetics"])
            copy_crn[i]["kinetics"] = res.x[i]

        return copy_crn, res.fun

    def globopt_mm(self, crn):

        """
        Global optimization performed after a CRN has been obtained from
        research tree for michaelis menten kinetics.
        - Input:
            * crn outputted by research tree.
        - Output:
            * crn with optimized parameters.
            * res.fun: quadratic loss after parameter fitting.
        """
        nreacs = len(crn)
        S = np.array([reac["skeleton"] for reac in crn])

        nvmax, nkm, mm_or_mal = len(crn), 0, np.zeros(len(crn))
        mm_or_mal = [1 if r["km"] else 0 for r in crn]
        nkm = len([m for m in mm_or_mal if m])

        bounds = tuple([(0, None) for _ in range(nvmax + nkm)])
        if nreacs:
            res = minimize(
                compute_energy_mm,
                np.random.uniform(0, 1, nvmax + nkm),
                args=(self.vinit, S, mm_or_mal, crn),
                method="L-BFGS-B",
                bounds=bounds,
                options={"ftol": 10 ** (-12)},
            )
            if res.message == "ABNORMAL_TERMINATION_IN_LNSRCH":
                res = minimize(
                    compute_energy_mm,
                    np.random.uniform(0, 1, nvmax + nkm),
                    args=(self.vinit, S, mm_or_mal, crn),
                    method="trust-constr",
                    options={"gtol": 10 ** (-11)},
                    bounds=bounds,
                )
        copy_crn, count = copy.deepcopy(crn), 0
        for i in range(len(copy_crn)):
            copy_crn[i]["kinetics"] = res.x[i]
            if copy_crn[i]["km"]:
                copy_crn[i]["km"] = res.x[len(copy_crn) + count]
                count += 1

        return copy_crn, res.fun

    def kinetics_function(self, r, km=0):

        """
        Compute MAL kinetics or MM kinetics.
        - Input:
            * r: list of reactants.
            * km: Km parameter in case of MM reaction.
        - Output:
            * numpy vector of same length as self.y, reaction effect
        """
        return (
            self.y[:, r] / (km + self.y[:, r])
            if self.kinetics == "mm" and len(r) == 1
            else self.y[:, r].prod(axis=1, keepdims=True)
        )

    def get_energy_quantities(self, crn):

        """
        Compute quantities of interest for energy computation
        - Input:
            * crn: dict outputted by the research tree
        - Output:
            * S: stoichiometry matrix of the CRN
            * K: numpy vector of kinetic parameters
            * effect: numpy matrix of reaction effects
        """

        S = np.array([reac["skeleton"] for reac in crn])
        K = np.array([reac["kinetics"] for reac in crn])
        effect = np.hstack(
            [reac["effect"][0](reac["effect"][1], reac["km"]) for reac in crn]
        )

        return S, K, effect

    def plot_support(self, reac, tree, save=None, show=True):

        """
        Show the support of a reaction. Only works for one trace right now.
        - Input:
            * reac: reaction dict, element of a crn dict.
            * tree: search tree constructed by Reactmine.
            * save: str, file name to save the plot.
        """

        predecessors = [tree[i][j] for i, j in enumerate(reac["idx"][:-1])]
        supp = reac["supp"]
        v = np.copy(self.vinit)
        vold = np.copy(self.vinit)
        for pred in predecessors:
            v = self.transition_update(pred, v)
        v = v / np.abs(vold).max(axis=0)
        fig, axes = plt.subplots(1, 1, figsize=(10, 5))
        axes.plot(self.t[0], v, marker="o", markersize=3)
        for s in np.where(reac["skeleton"])[0]:
            axes.scatter(
                self.t[0][supp], v[supp, s], marker="o", s=20, color="black", zorder=10
            )
        strreac = str_reaction(
            self.species_names,
            reac["skeleton"],
            reac["kinetics"],
            reac["cata"],
            reac["km"],
            plot=True,
        )
        idx = strreac.find("∅")
        if idx > -1:
            strreac = strreac[:idx] + "$\emptyset$" + strreac[idx + 1 :]
        axes.set_title(f"Support of reaction {strreac}")
        fig.legend(
            self.species_names, shadow=True, ncol=1, bbox_to_anchor=(1.025, 0.75)
        )
        axes.set_xlabel("Time (arbitrary units)")
        axes.set_ylabel("Velocities $V$ (max normalized)")
        if save:
            adapt_save_fig(fig, f"{save}.pdf")
        plt.show() if show else None


def get_mean_cv(dist):

    """
    Compute mean estimate and unbiased estimate for coefficient of variation.
    - Input:
        * dist: numpy vector, distribution of estimated MAL parameters at each
        time point.
    - Output:
        * mean: scalar, mean.
        * cv: scalar, coefficient of variation.
    """

    # handle the case where mean is 0 or absence of samples
    if not dist.size:
        mean = 0.0
        nsamples = 1
    else:
        mean = dist.mean()
        nsamples = len(dist)

    cv = dist.std() / np.abs(mean) if mean else np.inf
    # unbiased estimator in the low sample regime
    cv *= 1 + 1 / (4 * nsamples)
    return mean, cv


def get_skeletons(v, delta):

    """
    Get reaction skeletons based on highest similarly-varying species.
    - Input:
        * v: numpy array, matrix of velocities.
        * delta: numpy vector of values containing different species matching thresholds.
    - Output:
        * skeleton_matrix: numpy matrix of candidate reaction skeletons.
        * support: dict whose keys are stoichiometry vectors representing a reaction skeleton
                   and values are numpy arrays of time points used to infer the
                   skeleton.
    """

    abs_v = np.abs(v)
    support, skeleton_matrix = {}, np.zeros([0, v.shape[1]])
    argmax_species = np.argmax(abs_v / abs_v.max(axis=0), axis=1)

    for d in delta:
        for i in range(len(v)):
            skeleton = (abs_v[i, argmax_species[i]] <= (d * abs_v[i])) * np.sign(v[i])
            skeltup = tuple(skeleton)
            if skeltup not in support.keys():
                if check_reaction(skeleton):
                    support[skeltup] = [i]
                    skeleton_matrix = np.vstack((skeleton_matrix, skeleton))
            elif skeltup in support.keys() and i not in support[skeltup]:
                support[skeltup].append(i)
    return skeleton_matrix, support


def check_reaction(skeleton):

    """
    Check whether an inferred reaction skeleton (stoichiometry vector)
    is elementary.
    - Input:
        * skeleton: stoichiometry vector of -1, 0 and 1s.
    - Output:
        * bool_reac: boolean, whether or not a reaction is elementary.
    """

    bool_reac = True
    nreac = len(skeleton[skeleton < 0])
    nprod = len(skeleton[skeleton > 0])
    nterms = nreac + nprod
    if nterms > 3 or nreac > 2 or nprod > 2 or not nterms:
        bool_reac = False
    return bool_reac


def compute_energy(K, v, effect, skeletons):

    """
    Compute quadratic loss between observed and inferred velocities.
    - Input:
        * K: numpy vector of size nreacs, kinetic parameters.
        * v: numpy matrix of velocities.
        * effect: numpy array (npts_stacked, nreacs) with each column storing a
        reaction effect.
        * skeletons: numpy matrix, stoichiometry matrix.
    - Output: quadratic loss.
    """

    residual = ((v - np.dot(effect * K, skeletons)) / np.abs(v).max(axis=0)) ** 2
    return np.sum(residual) / len(effect)


def compute_energy_mm(K, v, skeletons, mm_or_mal, crn):

    """
    Compute quadratic loss between observed and inferred velocities for Michaelis Menten kinetics.
    - Input:
        * K: numpy vector of size at most 2*nreacs, kinetic parameters.
        * v: numpy matrix of velocities.
        * skeletons: numpy matrix, stoichiometry matrix.
        * mm_or_mal: binary vector the size of the crn, True if the reaction is MM.
        * crn: crn.
    - Output: quadratic loss.
    """

    mmcopy = np.array(mm_or_mal, dtype=np.float64)
    weights = K[: len(skeletons)]
    mmcopy[mmcopy > 0] *= K[len(skeletons) :]
    effect = np.hstack(
        [reac["effect"][0](reac["effect"][1], m) for reac, m in zip(crn, mmcopy)]
    )
    residual = ((v - np.dot(effect * weights, skeletons)) / np.abs(v).max(axis=0)) ** 2
    return np.sum(residual) / len(effect)


def minimization(K, v, effect, S, bounds, method="L-BFGS-B"):

    """Wrapper for minimize to select optimization method."""
    return minimize(
        compute_energy,
        K,
        args=(v, effect, S),
        method=method,
        bounds=bounds,
    )
