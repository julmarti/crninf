"Parser, inference results print, plot settings and postprocessing."

import argparse
import copy
import itertools

import matplotlib as mpl
import numpy as np


def print_crn(crn, species, file=None):

    """
    Print learned CRN.
    - Input:
        * crn: dict output from Reactmine.
        * species_names: list of molecular species.
        * file: str or None, file name for print.
    """

    for n in range(len(crn)):
        reaction = crn[f"reaction {n}"]
        print(
            str_reaction(
                species,
                reaction["skeleton"],
                reaction["kinetics"],
                reaction["cata"],
                reaction["km"],
            ),
            file=file,
        )


def str_reaction(species, *reaction, plot=False):

    """
    Pretty print of a learned reaction.
    - Input:
        * species: list of molecular species.
        * reaction: either the dict entries 'vector', 'kinetics' and 'cata' if
        reaction was learnt by the algorithm, or the vector, kinetics and cata
        if the reaction is generated.
    - Output:
        * str, inferred reaction.
    - Example:
        print_reaction(algo.species_names,
                       algo.crn['reaction 0']['skeleton'],
                       algo.crn['reaction 0']['kinetics'],
                       algo.crn['reaction 0']['cata'])
    """

    reactants = np.where(reaction[0] == -1)[0]
    products = np.where(reaction[0] == 1)[0]
    k, cata = np.round(reaction[1], 5), reaction[2]
    km = np.round(reaction[-1], 5)

    left_term = ""
    right_term = ""

    if not reactants.size:
        left_term += "∅"
    elif not products.size:
        right_term += "∅"
    if reactants.size:
        if reactants.size == 1:
            left_term = species[reactants[0]]
        else:
            left_term = species[reactants[0]] + " + " + species[reactants[1]]
    if products.size:
        if products.size == 1:
            right_term = species[products[0]]
        else:
            right_term = species[products[0]] + " + " + species[products[1]]
    kinetics = f"MA({k})" if (len(reactants) != 1 or not km) else f"MM{(k,km)}"
    cata = "" if cata == -1 else f" with catalyst {species[cata]}"
    arrow = r" $\rightarrow$ " if plot else " -> "
    return kinetics + " for " + left_term + arrow + right_term + cata


def adapt_save_fig(fig, filename="test.pdf"):

    """Remove right and top spines, set bbox_inches and dpi."""

    for ax in fig.get_axes():
        ax.spines["right"].set_visible(False)
        ax.spines["top"].set_visible(False)
    fig.savefig(filename, bbox_inches="tight", dpi=300)


def set_matplotlib_params():

    """Set matplotlib params."""

    mpl.rcParams.update(mpl.rcParamsDefault)
    mpl.rc("font", family="serif")
    mpl.rcParams.update(
        {
            "font.size": 24,
            "lines.linewidth": 2,
            "axes.labelsize": 24,  # fontsize for x and y labels
            "axes.titlesize": 24,
            "xtick.labelsize": 20,
            "ytick.labelsize": 20,
            "legend.fontsize": 20,
            "axes.linewidth": 2,
            "pgf.texsystem": "pdflatex",  # change this if using xetex or lautex
            "text.usetex": True,  # use LaTeX to write all text
            "axes.spines.right": False,
            "axes.spines.top": False,
        }
    )


def low_complexity_crn(crnlist, energies, atol=10 ** (-3), rtol=1.1):

    """
    Compute the least complex CRN within a certain area of loss value
    - Input:
        * crnlist: list of crn dicts.
        * energies: list of associated energies.
        * atol: absolute tolerance to collect CRNs whose energy is close to the lowest one.
        * rtol: relative tolerance to collect CRNs whose energy is close to the lowest one.
    - Output:
        * bestcrn: least complex CRN.
    """

    idx_lowcomp = np.unique(
        np.append(
            np.where(energies < (atol + min(energies)))[0],
            np.where(energies < rtol * min(energies))[0],
        )
    )
    comp = [
        len(np.where([crnlist[j][i]["kinetics"] > 0 for i in crnlist[j].keys()])[0])
        for j in idx_lowcomp
    ]
    idx_best = idx_lowcomp[np.argmin(comp)]
    bestcrn = crnlist[idx_best]
    return remover_zero_reac(bestcrn) if len(bestcrn) > 1 else bestcrn, idx_best


def remover_zero_reac(crn):

    """Remove reactions with 0-valued kinetic parameter from input CRN."""

    cpt, copy_crn = 0, {}
    for i in range(len(crn)):
        if crn[f"reaction {i}"]["kinetics"]:
            copy_crn[f"reaction {cpt}"] = crn[f"reaction {i}"]
            cpt += 1
    return copy_crn


def remove_ABBA(crn):

    """Remove two opposite reactions such as A=>B and B=>A if their kinetics are 10% close from input CRN."""

    copy_crn = copy.deepcopy(crn)
    complete_skel = [np.append(reac["skeleton"], reac["cata"]) for reac in crn.values()]
    flag = []
    for i1, c1 in enumerate(complete_skel):
        for i2, c2 in enumerate(complete_skel[1:]):
            kin = [
                crn[f"reaction {i1}"]["kinetics"],
                crn[f"reaction {i2+1}"]["kinetics"],
            ]
            if (
                np.array_equal(c1, np.append(-c2[:-1], c2[-1]))
                and min(kin) > 0
                and 1 - (min(kin) / max(kin)) < 0.1
            ):
                flag.append(i1)
                flag.append(i2 + 1)
    flag = list(set(flag))
    for f in flag:
        del copy_crn[f"reaction {f}"]
    listkey = list(copy_crn.keys())
    for i, k in enumerate(listkey):
        copy_crn[f"reaction {i}"] = copy_crn.pop(k)
    return copy_crn


def merge(crn):

    """
    Merge couple of reactions such as (A => _) & (A => B + A) with similar MAL parameter values into A=>B (ODE-equivalence).
    """

    nspecies = len(crn["reaction 0"]["skeleton"])
    copy_crn = copy.deepcopy(crn)
    skeletons, catas = [i["skeleton"] for i in crn.values()], [
        i["cata"] for i in crn.values()
    ]
    liskel = [i["skeleton"].tolist() for i in crn.values()]
    kinetics = [i["kinetics"] for i in crn.values()]
    deg, synt = [], []
    for i, (j, c1) in enumerate(zip(skeletons, catas)):
        if j.sum() == -1 and np.abs(j).sum() == 1 and c1 == -1:
            deg.append([np.where(j == -1)[0][0], i])

    for i, (j, c2) in enumerate(zip(skeletons, catas)):
        if j.sum() == 1 and np.abs(j).sum() == 1 and c2 in [d[0] for d in deg]:
            synt.append([np.where(j == 1)[0][0], i, c2])

    combi = list(itertools.product(*[deg, synt]))
    add = 0
    copy_reac = copy.deepcopy(crn["reaction 0"])
    deletes = []
    for a in combi:
        if a[0][0] == a[1][2]:
            r1, r2 = a[0][1], a[1][1]
            kin = [kinetics[k] for k in [r1, r2]]
            if min(kin) > 0 and 1 - (min(kin) / max(kin)) < 0.1:
                deletes.append(r1)
                del copy_crn[f"reaction {r2}"]
                skel = np.zeros(nspecies)
                skel[a[0][0]] = -1
                skel[a[1][0]] = 1
                if skel.tolist() not in liskel:
                    copy_crn[f"new{add}"] = copy.deepcopy(copy_reac)
                    copy_crn[f"new{add}"]["cata"] = -1
                    copy_crn[f"new{add}"]["skeleton"] = skel
                add += 1
    for r in list(set(deletes)):
        del copy_crn[f"reaction {r}"]
    return copy_crn


def parser_example():

    """
    Parser used to run the algorithm from an already known crn for evaluation purposes.
    - Output:
        * parser: ArgumentParser object.
    """

    parser = argparse.ArgumentParser(description="Command description.")
    parser.add_argument(
        "-c",
        "--crn_name",
        default=None,
        help="str, crn name. Can be 'chain', 'loop', 'product-parallel', 'reactant-parallel', 'mapk', 'lv'. ",
        type=str,
    )
    parser.add_argument(
        "-s", "--save", default=None, type=str, help="Save the results on a dict."
    )
    parser.add_argument(
        "-d",
        "--delta_max",
        default=3,
        type=float,
        help="delta max species variation pairing",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Verbose for algorithm",
        default=None,
        action="store_true",
    )
    parser.add_argument(
        "-a",
        "--alpha",
        help="float, coefficient of variation threshold",
        default=0.5,
        type=float,
    )
    parser.add_argument(
        "-b",
        "--beta",
        help="int, number of candidates",
        default=6,
        type=int,
    )
    parser.add_argument(
        "-g",
        "--gamma",
        help="int, crn size limit",
        default=6,
        type=int,
    )
    parser.add_argument(
        "-npts",
        "--npts",
        help="int, number of time points",
        default=101,
        type=int,
    )
    parser.add_argument(
        "-t",
        "--time",
        help="float, simulation time horizon",
        default=10,
        type=float,
    )
    parser.add_argument(
        "-k",
        "--kinetics",
        help="str, kinetic rate functions",
        default="mal",
        type=str,
    )
    return parser
