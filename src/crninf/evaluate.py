"""Functions needed to create a benchmarking and evaluation protocol."""

import numpy as np
from scipy.integrate import odeint


def precision_recall(learned_crn, gt_crn, gt_cata):

    """
    Compute precision and recall based on ground truth
    and inferred reactions.
    - Input:
        * learned_crn: dict, crn learned by the algorithm.
        * gt_crn: numpy matrix size (n_true_reacs, nspecies), ground truth crn.
        * gt_cata: numpy vector size (n_true_reacs), ground truth catalysts.
    - Output:
        * precision: float in [0, 1].
        * recall: float in [0, 1].
    """

    gt_list = gt_crn.tolist()
    TP, FP, FN = 0, 0, 0
    precision, recall = 0, 0
    if len(learned_crn):
        for reac in learned_crn.values():
            # kinetics updated to 0
            if reac["kinetics"]:
                if reac["skeleton"].tolist() in gt_list:
                    index = gt_list.index(reac["skeleton"].tolist())
                    if reac["cata"] == gt_cata[index]:
                        TP += 1
                    else:
                        FP += 1
                else:
                    FP += 1
                FN = len(gt_crn) - TP
                precision, recall = TP / (TP + FP), TP / (TP + FN)
    return precision, recall


def f1_score(learned_crn, gt_crn, gt_cata):

    """
    Compute F1-score.
    - Input:
        * learned_crn: dict, crn learned by the algorithm.
        * gt_crn: numpy matrix size (n_true_reacs, nspecies), ground truth crn.
        * gt_cata: numpy vector size (n_true_reacs), ground truth catalysts.
    - Output:
        * f1_score: float in [0, 1].
    """

    precision, recall = precision_recall(learned_crn, gt_crn, gt_cata)
    return (
        precision,
        recall,
        2 * (precision * recall) / (precision + recall) if precision + recall else 0,
    )


def get_sde(S, K, cata):

    """
    Map a CRN to a system of differential equations.
    - Input:
        * S: list of reaction skeletons.
        * K: list of MAL parameters.
        * cata: list of reaction catalysts.
    - Output:
        * dsdt: dict.
    """

    nreacs, nspecies = len(S), len(S[0])
    dsdt = {}
    for ns in range(nspecies):
        dsdt[f"ds_{ns}/dt"] = []
    for nr in range(nreacs):
        R = np.where(S[nr] == -1)[0]
        P = np.where(S[nr] == 1)[0]
        Rc = np.r_[R, int(cata[nr])] if cata[nr] != -1 else R
        for r in R:
            dsdt[f"ds_{r}/dt"].append((Rc, -K[nr]))
        for p in P:
            dsdt[f"ds_{p}/dt"].append((Rc, K[nr]))
    return dsdt


def sde(y, t, eqs):

    """Callable for scipy.integrate.odeint."""

    list_ode = []
    for eq in eqs.values():
        terms = 0
        for e in eq:
            terms += np.prod(y[e[0]]) * e[1]
        list_ode.append(terms)
    return list_ode


def predict(crn, y0, t):

    """
    Numerical integration of the learned CRN.
    - Input:
        * crn: dict of inferred reactions.
        * y0: numpy vector of initial conditions.
        * t: time grid for numerical integration.
    - Output:
        numerical integration of the learned CRN, predicted derivatives.
    """

    S = [reac["skeleton"] for reac in crn.values()]
    K = [reac["kinetics"] for reac in crn.values()]
    C = [reac["cata"] for reac in crn.values()]
    effect = np.hstack(
        [reac["effect"][0](reac["effect"][1], reac["km"]) for reac in crn.values()]
    )

    return odeint(sde, y0, t, args=(get_sde(S, K, C),)), np.dot(effect * K, S)
