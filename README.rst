========
Overview
========

This repository contains the implementation of Reactmine, based on the submitted publication
Reactmine: an algorithm for inferring biochemical reaction networks from time series data.
It also features one tutorial notebook and a notebook reproducing some of the results of the article,
in the ``examples`` folder.

* Free software: GNU Lesser General Public License v3 (LGPLv3)

Installation
============

::

    python3 -mpip install crninf

Or you can clone this repo and run ``python3 -mpip install .`` from the root.

Development
===========

To run all the tests run::

    tox