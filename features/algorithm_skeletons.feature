Feature: Algorithm
    Algorithm stage

Scenario: Compute reaction skeletons
    Given velocities
    And classical skeletons and their supports
    When compute skeletons with delta max parameter 3.0 and delta step 0.1
    Then obtain a matrix of skeletons
    And classical skeletons and their supports are included