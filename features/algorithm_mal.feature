Feature: Algorithm
    Algorithm stage

Scenario: Compute mass action law parameter
    Given time series data
    And a Reactmine object
    And a reaction skeleton
    When compute mass action law parameter
    Then outputted dict values are healthy
