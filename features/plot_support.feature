Feature: Plot
    Plot support

Scenario: Plot support of inferred reactions
    Given time series data
    And reactmine output
    When plot support of reactions
    Then plot is saved
