"""Reactmine algorithm feature tests."""

from pathlib import Path  # noqa: F401

import numpy as np
from pytest_bdd import given, scenario, then, when

from crninf.algorithm import Reactmine


@scenario("algorithm_mal.feature", "Compute mass action law parameter")
def test_compute_mass_action_law_parameter():
    """Compute mass action law parameter."""


@given("time series data", target_fixture="data")
def time_series_data():
    """time series data"""
    return np.loadtxt(Path("./tests/test_data/chain.txt")), np.linspace(0, 10, 101)


@given("a Reactmine object", target_fixture="algo")
def a_Reactmine_object(data):
    """a Reactmine object."""
    concentrations, time = data
    return Reactmine([concentrations], [time], kinetics="mal")


@given("a reaction skeleton", target_fixture="skeleton")
def a_reaction_skeleton():
    """a reaction skeleton and associated support."""
    return (np.array([-1, 1, 0, 0, 0]), [i for i in range(10)])


@when("compute mass action law parameter", target_fixture="reaction")
def compute_mass_action_law_parameter(algo, skeleton):
    """compute mass action law parameter"""
    skeleton, support = skeleton
    return algo.get_kinetics(skeleton, algo.v, support)


@then("outputted dict values are healthy")
def outputted_dict_values_are_healthy(reaction, skeleton):
    """outputted dict values are healthy"""
    skeleton, support = skeleton
    effect = reaction["effect"][0]
    assert np.array_equal(reaction["skeleton"], skeleton)
    assert reaction["kinetics"] > 0
    assert reaction["cv"] != np.inf
    assert reaction["km"] == 0
    assert reaction["supp"] == support
    assert (effect([0]) > 0).all()
