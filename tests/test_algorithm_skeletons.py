"""Reactmine algorithm feature tests."""

from pathlib import Path  # noqa: F401

import numpy as np
from pytest_bdd import given, parsers, scenario, then, when

import pickle as pkl
from crninf.algorithm import get_skeletons


@scenario("algorithm_skeletons.feature", "Compute reaction skeletons")
def test_compute_reaction_skeletons():
    """Compute reaction skeletons."""


@given("velocities", target_fixture="v")
def velocities():
    """velocities"""
    return np.loadtxt(Path("./tests/test_data/chain_dxdt.txt"))


@given("classical skeletons and their supports", target_fixture="classics")
def classical_skeletons_and_their_supports():
    """classical skeletons and their supports"""
    with open(Path("./tests/test_data/supports_chain.dat"), "rb") as f:
        classics = pkl.load(f)
    return classics


@when(
    parsers.parse(
        "compute skeletons with delta max parameter {delta_max:f} and delta step {delta_step:f}"
    ),
    target_fixture="skeletons",
)
def compute_skeletons_with_delta_max_parameter(delta_max, delta_step, v):
    """compute skeletons"""
    delta = np.arange(1, delta_max + delta_step, delta_step)
    return get_skeletons(v, delta)


@then("obtain a matrix of skeletons")
def obtain_a_matrix_of_skeletons(v, skeletons):
    """obtain a matrix of skeletons"""
    skeletons, _ = skeletons
    assert skeletons.shape[1] == v.shape[1]
    assert set(skeletons.ravel()) == set([-1.0, 0.0, 1.0])


@then("classical skeletons and their supports are included")
def classical_skeletons_and_their_supports_are_included(skeletons, classics):
    """classical skeletons and their supports are included"""
    _, supports = skeletons
    assert supports == classics
