"""StatCRI algorithm feature tests."""

from pathlib import Path  # noqa: F401

import numpy as np
from pytest_bdd import given, scenario, then, when

from crninf.algorithm import Reactmine


@scenario("plot_support.feature", "Plot support of inferred reactions")
def test_plot_support_of_inferred_reactions():
    """Plot support of inferred reactions."""


@given("time series data", target_fixture="data")
def time_series_data():
    """time series data"""
    return np.loadtxt(Path("./tests/test_data/chain.txt")), np.linspace(0, 10, 101)


@given("reactmine output", target_fixture="output")
def reactmine_output(data):
    """reactmine output"""
    concentrations, time = data[0], data[1]
    algo = Reactmine(
        [concentrations], [time], kinetics="mal", alpha=0.02, beta=4, gamma=3
    )
    return [algo.fit(), algo]


@when("plot support of reactions", target_fixture="skeletons")
def plot_support_of_reactions(output, tmpdir):
    """plot support of reactions"""
    output, algo = output
    tree, crnlist, _ = output
    [
        algo.plot_support(
            crnlist[0][f"reaction {i}"], tree, save=f"{tmpdir}/test{i}", show=False
        )
        for i in range(len(crnlist[0]))
    ]


@then("plot is saved")
def plot_is_saved(tmpdir):
    """plot is saved"""
    for i in range(3):
        assert (Path(tmpdir) / f"test{i}.pdf").is_file()
